from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
  
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///blog.db'
db = sqlalchemy(app)

class Blog(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    author = db.Column(db.Text)
    content = db.Column(db.Text)
    title = db.Column(db.Text)

    def __init__(self, title, content, author):
        self.content = content
        self.title = title 
        self.author = author

db.create_all()
